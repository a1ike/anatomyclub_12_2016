jQuery(document).ready(function() {

  //fixed scroll header	
  $(window).scroll(function() {
    if ($(this).scrollTop() > 300) {
      $('.navbar-static-top').addClass('navbar-scroll');
    } else {
      $('.navbar-static-top').removeClass('navbar-scroll');
    }
  });

  $('#owl-demo').owlCarousel({

    navigation: false, // Show next and prev buttons
    slideSpeed: 300,
    pagination: false,
    paginationSpeed: 400,
    singleItem: true

    // "singleItem:true" is a shortcut for:
    // items : 1, 
    // itemsDesktop : false,
    // itemsDesktopSmall : false,
    // itemsTablet: false,
    // itemsMobile : false

  });

});